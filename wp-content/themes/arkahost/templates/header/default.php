<?php
/**
*	This file has been preloaded, so you can wp_enqueue_style to out in wp_head();
*/	

	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

	global $king, $king_whmcs;
	
	wp_enqueue_style('king-menu');
	
?>
<!--Header default-->
<div class="top_nav">
	<div class="container">
	    <div class="left">
		   	<?php  if( !empty( $king->cfg['topSocials'] ) ){ 
		    	$king->socials( 'topsocial', 5 );
	        } ?>
	    </div><!-- end left -->
	    
	    <div class="right">
	       
	    <?php 
		    
		    global $woocommerce;
		    
		    if( empty( $king->cfg['topInfoCart'] ) ){
				$king->cfg['topInfoCart'] = 'show';
			}
		    
		    if( $king->cfg['topInfoCart'] == 'show' && !empty( $woocommerce ) ){ 
			    
		?>
	        <div  class="tpbut two minicart-li">
		        <a href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" class="minicart-nav">
			        <i class="et-basket et"></i>
			        <span class="cart-items"><?php echo WC()->cart->cart_contents_count; ?></span>
			    </a>    
		        <ul class="dropdown-menu">
					<li><?php  
						if( function_exists( 'king_cart_func' ) ){
							echo '<div class="minicart-wrp">'.king_cart_func( array() ).'</div>';
						}
					?></li>
				</ul>
	        </div>
		<?php }
		
			if( !empty( $king->cfg['topInfoLogin'] ) ){ ?>
				<?php if($king->cfg['topInfoLogin'] == 'wp'): ?>
					<?php if ( is_user_logged_in() ) { ?>
						<a href="<?php echo get_edit_profile_url(); ?>" class="tpbut">
							<i class="fa fa-user"></i>&nbsp; <?php _e( 'My Profile', 'arkahost' ); ?>
						</a>
					<?php }else{ ?>
						<a href="<?php echo SITE_URI; ?>/?action=login" class="tpbut">
							<i class="fa fa-user"></i>&nbsp; <?php _e( 'Login', 'arkahost' ); ?>
						</a>
					<?php } ?>
				<?php elseif($king->cfg['topInfoLogin'] == 'whmcs'): ?>
					<?php if($king_whmcs->is_client_loggedin()): ?>
						<a href="<?php echo get_permalink($king_whmcs->get_bridge_page_id()); ?>?ccce=clientarea" class="tpbut bridge_link">
							<i class="fa fa-user"></i>&nbsp; <?php _e( 'Client Area', 'arkahost' ); ?>
						</a>
					<?php else: ?>
						<a href="<?php echo get_permalink($king_whmcs->get_bridge_page_id()); ?>?ccce=clientarea" class="tpbut bridge_link">
							<i class="fa fa-user"></i>&nbsp; <?php _e( 'Login', 'arkahost' ); ?>
						</a>
					<?php endif; ?>
				<?php endif; ?>
	    <?php } ?>
	    
	        <ul class="tplinks">
		        <?php if( !empty( $king->cfg['topInfoPhone'] ) ){ ?>
	            <li>
	            	<strong>
	            		<i class="fa fa-phone"></i> <?php echo esc_html( $king->cfg['topInfoPhone'] ); ?>
	            	</strong>
	            </li>
	            <?php }if( !empty( $king->cfg['topInfoEmail'] ) ){ ?>
	            <li>
	            	<a href="mailto:<?php echo esc_attr( $king->cfg['topInfoEmail'] ); ?>">
		            	<img src="<?php echo THEME_URI; ?>/assets/images/site-icon1.png" alt="" /> 
		            	<?php _e( 'WebMail', 'arkahost' ); ?>
		            </a>
		        </li>
		        <?php }if( !empty( $king->cfg['topInfoLiveChat'] ) ){ ?>
	            <li>
	            	<a href="<?php echo esc_url( $king->cfg['topInfoLiveChat'] ); ?>">
		            	<img src="<?php echo THEME_URI; ?>/assets/images/site-icon2.png" alt=""> 
		            	<?php _e( 'LiveChat', 'arkahost' ); ?>
		            </a>
		        </li>
		        <?php }if( !empty( $king->cfg['topInfoSupport'] ) ){ ?>
	            <li>
	            	<a href="<?php echo esc_url( $king->cfg['topInfoSupport'] ); ?>">
		            	<img src="<?php echo THEME_URI; ?>/assets/images/site-icon3.png" alt=""> 
		            	<?php _e( 'Support', 'arkahost' ); ?>
		            </a>
		        </li>
		        <?php } ?>
	        </ul>    
	    </div><!-- end right --> 
	</div>
</div>
<div class="clearfix"></div>
<header class="header">
	<div class="container">
	    <!-- Logo -->
	    <div class="logo">
		    <a href="<?php echo esc_url(home_url('/')); ?>" id="logo">
	    		<img src="<?php echo esc_url( $king->cfg['logo'] ); ?>" alt="<?php bloginfo('description'); ?>" />
			</a>
	    </div>
		<!-- Navigation Menu -->
	    <div class="menu_main">
	      <div class="navbar yamm navbar-default">
	          <div class="navbar-header">
	            <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"> 
	              <span><?php _e( 'Menu', 'arkahost' ); ?></span>
	              <button type="button"> <i class="fa fa-bars"></i></button>
	            </div>
	          </div>
	          <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
	            <nav><?php $king->mainmenu(); ?></nav>
	          </div>
	      </div>
	    </div>
	<!-- end Navigation Menu -->
	</div>    
</header>
<div class="clearfix margin_bottom11 resp_margin_bottom68"></div>