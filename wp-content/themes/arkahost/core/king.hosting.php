<?php
/*
	Author: King Theme
*/

class King_Hosting{
      
    protected $whois = array();
		
	//Max request search domain in ($time_for_request) second
	public $max_request = 3;
	public $time_for_request = 10;
	protected $request_count;
	
	/*-----------------------------
	 function __construct()
	 Run for default
	 ----------------------------*/
    public function __construct($debug = false){
	    
		if( !isset($_SESSION) ){
			session_start();
		}		
		
		global $king;
						
		$this->init();		
		$this->whois = $this->whois();		
    }
	
	
	/*-----------------------------
	 function init()
	 ----------------------------*/
	public function init(){
		
		global $king;
				
		add_action('wp_head', array(&$this, 'vars_js'));
		add_action('wp_ajax_nopriv_king_search_domain', array(&$this, 'ajax_search_domain'));
		add_action('wp_ajax_king_search_domain', array(&$this, 'ajax_search_domain'));
		
		add_action('wp_ajax_nopriv_king_get_whois', array(&$this, 'ajax_get_whois'));
		add_action('wp_ajax_king_get_whois', array(&$this, 'ajax_get_whois'));
		
		add_action( 'wp_enqueue_scripts', array(&$this, 'enqueue_script'), 1 );		
	}

	
	public function vars_js(){
		
		$js_dir = THEME_URI.'/assets/js/'; 
		
		echo '<script type="text/javascript">
		/* <![CDATA[ */
		var king_hosting_params = {"home_url":"'. home_url() .'", "ajax_url":"'. admin_url('admin-ajax.php') .'", "hosting_js":"'.$js_dir.'king.hosting.js"};
		/* ]]> */
		</script>';
	}
	
	/*-----------------------------
	 Add new script and style for hosting function
	 ----------------------------*/
	public function enqueue_script(){
		
		global $king;

		$css_dir = THEME_URI.'/assets/css/';
		$js_dir = THEME_URI.'/assets/js/';
		
		wp_enqueue_style('king-hosting', king_child_theme_enqueue( $css_dir.'king-hosting.css'  ), false, KING_VERSION );
		
		wp_register_script('king-hosting', king_child_theme_enqueue( $js_dir.'king.hosting.js' ), false, KING_VERSION, true );
		wp_enqueue_script('king-hosting');
		
	}
	
	/*------------------------------------
	 Get whois server
	 -----------------------------------*/
	public function whois(){
		if ( empty( $this->whois ) ) {
			locate_template( 'core'.DS.'inc'.DS.'whois-server.php', true  );
			$this->whois = apply_filters( 'king_theme_whois_server', king_get_whois_server() );	
		}
		return $this->whois;
	}
	
	/*------------------------------------	 
	Get whois info	 
	-----------------------------------*/	
	public function get_whois_info($domain, $echo = false){		
		if($echo){
			echo $this->server_response($domain);
		}else{
			return $this->server_response($domain);
		}
	}
			
	/*------------------------------------	 
	Check is tld supported	 
	Return @true|false	 
	-----------------------------------*/
	public function is_tld_supported($domain){
		$_tld = $this->getTld($domain);
		
		$tld_supported = array();
		foreach($this->whois as $tld => $info){
			$tld_supported[] = $tld;
		}
		
		if(in_array($_tld, $tld_supported))
			return true;
		return false;
	}
	
	/*------------------------------------
	 Get response from server
	 -----------------------------------*/
	public function server_response($domain){
		global $king;
		
		$domain = strtolower($domain);
		
		$server = $this->whois[$this->getTld($domain)][0];

		$connection = $king->ext['fso']($server, 43);
		if (!$connection) return false;
		
		fputs($connection, $domain."\r\n");
		
		$response_text = ' ';
		while(!feof($connection)) {
			$response_text .= @fgets($connection,128).'<br />';
		}
		
		$king->ext['fc']($connection);
		
		return $response_text;
	}
	
	/*------------------------------------
	 Check available domain
	 @return true|false
	 -----------------------------------*/
    public function is_domain_available($domain){
        global $king;
		
		$response_text = $this->server_response($domain);
		
		if (strpos($response_text, $this->whois[$this->getTld($domain)][1])) return true;
		else return false;
    }

   
	/*------------------------------------
	 Get Tld
	 -----------------------------------*/
    public function getTld($domain)
    {
        return pathinfo($domain, PATHINFO_EXTENSION);
    }
	
	
	public function ajax_get_whois(){
		$domain = $_POST['domain'];
		
		$results_html = $this->get_whois_info($domain);
		
		$output = array(
			'status' 		=> 'ok',
			'results_html' 	=> $results_html
		);
		
		wp_send_json($output);
	}
	
	/*------------------------------------
	 ajax_search_domain()
	 -----------------------------------*/
	public function ajax_search_domain(){
		check_ajax_referer( 'ajax-check-domain-nonce', 'security' );
		
		$domain = $_POST['domain'];
		
		//Protected request check domain
		$this->protected_search_domain();
		
		if(!$this->is_tld_supported($domain)){
			$results_html = '<div class="content-result">
				<strong class="_dm-r00 domain-not-support">Sorry, that name is not available for registration. Please try again.</strong>
			</div>';
						
			$output = array(
				'status' 		=> 'Not support',
				'tld' 			=> $this->getTld($domain),
				'results_html'	=> $results_html
			);
			
			wp_send_json($output);
			
			die();
		}
		
						
		$results_html = '';
		$url_action = rtrim(get_permalink(get_option("cc_whmcs_bridge_pages")),"/")."/?ccce=domainchecker";
		$url_verifyimage = rtrim(get_permalink(get_option("cc_whmcs_bridge_pages")),"/")."/?ccce=verifyimage";
		
		if($this->is_domain_available($domain)){
			$status = 'available';
			$results_html .= '<div class="content-result">
				<strong class="_dm-r00 domain-available">Yes! '.$domain.' is available. Buy it before someone else does.</strong>
				<form class="select_domain" method="POST" action="'. $url_action .'">
					<input type="hidden" value="'.$domain.'" name="domain" class="bigfield">
					<!--<img align="middle" src="'. $url_verifyimage .'"> <input type="text" maxlength="5" class="input-small" name="code">-->
					<input class="input_submit" type="submit" name="select_domain" value="Select domain" />
				</form>
			</div>';
		}else{
			$status = 'taken';
			$results_html .= '<div class="content-result">
				<strong class="_dm-r00 domain-taken">Sorry, <span>'. $domain .'</span> is taken. <a class="whois_view" href="javascript:;" data-domain="'.$domain.'">Whois</a></strong>
				<div class="suggest_domain">';
				
				$results_html .= '<div class="title"><h3>You may want to check:</h3></div>';
				
				if(count($this->suggest_domain($domain))){
					foreach($this->suggest_domain($domain) as $d){
						$_whois = '';
						
						if($d['status'] == 'taken'){
							$_whois = '<a class="whois_view" href="javascript:;" data-domain="'.$d['domain'].'">Whois</a>';
						}else{
							$_whois = '<a class="select_this_domain" href="javascript:;" data-domain="'.$d['domain'].'">Select</a>';
						}
						
						$results_html .= '<div class="domain domain-'. esc_attr($d['status']) .'">
							<strong>'. $d['domain'] .'</strong> <span class="status '.$d['status'].'">'. $d['status'] .'</span> <div class="view_whois">'. $_whois .'</div>
						</div>';
					}
				}	
				$results_html .= '</div>
				<form id="select_this_domain" class="select_domain" method="POST" action="'. $url_action .'">
					<input type="hidden" value="'.$domain.'" name="domain" class="bigfield">
					<!--<img align="middle" src="'. $url_verifyimage .'"> <input type="text" maxlength="5" class="input-small" name="code">-->
				</form>
			</div>';
		}
				
		$output = array(
			'status' 		=> $status,
			'results_html' 	=> $results_html,
			'request_count' => $_SESSION['domain_request_count']
		);
		
		wp_send_json($output);
	}
	
	
	/*------------------------------------
	 suggest_domain()
	 -----------------------------------*/
	public function suggest_domain($domain){
		$current_tld = '.'.$this->getTld($this->url_to_domain($domain));
		$basename = $this->get_basename($domain);
		
		$sg_tld_arr = array('.com', '.net', '.org', '.info', '.us', '.biz');
		
		$suggest_domain_arr = array();
		
		foreach($sg_tld_arr as $tld){
			if($current_tld != $tld){
				$sg_domain = $basename.$tld;
								
				if($this->is_domain_available($sg_domain)){					
					$suggest_status = 'available';
				}else{
					$suggest_status = 'taken';
				}
				
				$suggest_domain_arr[] = array(
					'domain' => $sg_domain,
					'status' => $suggest_status
				);
			}		
		}
		
		return $suggest_domain_arr;
	}
	
	
	/*------------------------------------
	 url_to_domain()
	 -----------------------------------*/
	public function url_to_domain($url){
		$host = @parse_url($url, PHP_URL_HOST);		
		if (!$host)
			$host = $url;		
		if (substr($host, 0, 4) == "www.")
			$host = substr($host, 4);		
		if (strlen($host) > 50)
			$host = substr($host, 0, 47) . '...';
		return $host;
	}
	
	
	/*------------------------------------
	 get_basename()
	 -----------------------------------*/
	public function get_basename($url_domain){
		$domain = $this->url_to_domain($url_domain);
		return basename($domain, '.'.$this->getTld($domain));
	}
	
	/*------------------------------------
	 protected_search_domain()
	 -----------------------------------*/
	private function protected_search_domain(){
		global $king;
		
		if(!isset($_SESSION['domain_request_count'])){
			$_SESSION['domain_request_count'] = 0;
		}
		
		if( !isset($_SESSION['search_first_time']) ){			
			$_SESSION['search_first_time'] = time();
		}else{
			if(time() - $_SESSION['search_first_time'] < $this->time_for_request){
				$_SESSION['domain_request_count'] = intval($_SESSION['domain_request_count']) + 1;				
			}else{
				$_SESSION['domain_request_count'] = 0;
				$_SESSION['search_first_time'] = time();
			}
		}

		if($_SESSION['domain_request_count'] > $this->max_request){
			//Code ban ip and exit
			
			$output = array(
				'status' 		=> 'your ip banned!',
				'reason' 		=> 'too much request in short time.',
				'request_count' => $_SESSION['domain_request_count'].'/'.$this->time_for_request.'s'
			);
			
			wp_send_json($output);
			
			die();
		}		
	}
	
}


class King_Whmcs{
	
	public $session;
	
	public function __construct($debug = false){
		
		$this->session = $_SESSION;
		
		$cc_whmcs_bridge_version=get_option("cc_whmcs_bridge_version");
		if ($cc_whmcs_bridge_version) {
			remove_filter('the_content', 'cc_whmcs_bridge_content', 10);
			add_filter('the_content', array($this, 'whmcs_bridge_content'), 10, 3);
		}
	}
	
	
	public function whmcs_bridge_content($content) {
		global $cc_whmcs_bridge_content,$post;

		if (!is_page()) return $content;

		$_content = $content;

		$cf=get_post_custom($post->ID);
		if (isset($_REQUEST['ccce']) || (isset($cf['cc_whmcs_bridge_page']) && $cf['cc_whmcs_bridge_page'][0]==WHMCS_BRIDGE_PAGE)) {
			if (!$cc_whmcs_bridge_content) { //support Gantry framework
				$cc_whmcs_bridge_content=cc_whmcs_bridge_parser();
			}
			if ($cc_whmcs_bridge_content) {
				$content='';
				ob_start();
				if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('whmcs-top-page') ) :
				endif;
				$content.=ob_get_clean();
				$content.='<div id="bridge">';
				$content.=$cc_whmcs_bridge_content['main'];
				$content.='</div><!--end bridge-->';
				ob_start();
				if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('whmcs-bottom-page') ) :
				endif;
				$content.=ob_get_clean();
				if (get_option('cc_whmcs_bridge_footer')=='Page') $content.=cc_whmcs_bridge_footer(true);								
			}
			if(!isset($_REQUEST['ccce'])) $content .= $_content;
		}
		
		return $content;
	}
	
	
	public function is_client_loggedin(){
		if($this->is_bridge_actived() && $this->is_bridge_sso_actived()){
			$_session = $this->session;
			
			if(isset($_session['whmcs-bridge-sso']['cookie-array'])){
				$whmcs_cookie = $_session['whmcs-bridge-sso']['cookie-array'];
			}else{
				$whmcs_cookie = array('WHMCSUser' => '');
			}
			
			if(isset($whmcs_cookie['WHMCSUser'])){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	public function get_bridge_page_id(){
		$whmcs_page_id = get_option("cc_whmcs_bridge_pages");
		
		if(isset($whmcs_page_id)) return $whmcs_page_id;
		else return false;
	}
	
	public function is_bridge_actived(){
		return $this->is_plugin_active('whmcs-bridge/bridge.php');
	}
	
	public function is_bridge_sso_actived(){
		return $this->is_plugin_active('whmcs-bridge-sso/sso.php');
	}
	
	public function is_plugin_active($plugin){
		if ( in_array( $plugin , apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
			return true;
		}
		else {
			return false;
		}
	}
	
}

new King_Hosting();
global $king_whmcs;
$king_whmcs = new King_Whmcs();