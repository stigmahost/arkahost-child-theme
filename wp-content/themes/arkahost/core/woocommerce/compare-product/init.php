<?php
/**
* Init
*/
if ( !defined( 'ABSPATH' ) ) { exit; } // Exit if accessed directly
    define( 'king_WOOCOMPARE_URL', get_template_directory_uri() . '/core/woocommerce/compare-product/' );
    define( 'king_WOOCOMPARE_DIR', get_template_directory_uri() . '/core/woocommerce/compare-product/' );
	global $king, $king_woocompare;
    // Load required classes and functions
    locate_template( 'core'.DS.'woocommerce'.DS.'compare-product'.DS.'woocompare-frontend.php', true );
    locate_template( 'core'.DS.'woocommerce'.DS.'compare-product'.DS.'woocompare-widget.php', true );
    locate_template( 'core'.DS.'woocommerce'.DS.'compare-product'.DS.'woocompare.php', true );

    // Let's start the game!
    $king_woocompare = new king_Woocompare();