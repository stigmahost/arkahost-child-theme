<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

if ( 'www.stigmahost.wpl' === $_SERVER['HTTP_HOST'] ) {
// ** MySQL settings - You can get this info from your web host ** //
	/** The name of the database for WordPress */
	define( 'DB_NAME', 'stigmahost_new' );

	/** MySQL database username */
	define( 'DB_USER', 'root' );

	/** MySQL database password */
	define( 'DB_PASSWORD', 'root' );

	/** MySQL hostname */
	define( 'DB_HOST', 'localhost' );

	/** Database Charset to use in creating database tables. */
	define( 'DB_CHARSET', 'utf8mb4' );

	/** The Database Collate type. Don't change this if in doubt. */
	define( 'DB_COLLATE', '' );
} else {
	/** The name of the database for WordPress */
	define( 'DB_NAME', 'gaf_sigmahost' );

	/** MySQL database username */
	define( 'DB_USER', 'gaf_fZaQAuow' );

	/** MySQL database password */
	define( 'DB_PASSWORD', '(3l&B_u*esL?' );

	/** MySQL hostname */
	define( 'DB_HOST', 'localhost' );

	/** Database Charset to use in creating database tables. */
	define( 'DB_CHARSET', 'utf8mb4' );

	/** The Database Collate type. Don't change this if in doubt. */
	define( 'DB_COLLATE', '' );
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_@lM{b#-xC_.h+Hj-&|{dw7]6#2xzA~CX$z+8XPDyo?u-ewD2G,/nPX`L12 R>E|');
define('SECURE_AUTH_KEY',  '[YTn|9u v:ENxkVD!vh%;H=jq7{Zc&`8d3}.-tf|GtO$9.Zz)0zDITc--+^+t#9U');
define('LOGGED_IN_KEY',    'BWNyxbCID&Lbeq+z}/l|{U|YC-,aS3W|T3<QEt;JClYF:9;,,xrR>4*;<yp(I[T%');
define('NONCE_KEY',        '-s|Z0<zTi6.avw[c|-5mr#<WXKy_iY=UI^:;LQizvcB {^&5+$TMVF>=PY_x|#J|');
define('AUTH_SALT',        'Btx20eaf&ijL GyQ&gtz(;TB4Wy2/+D/fp9@2{IR{Y)6Mi![OQ.#KhFuXiTr=+Rl');
define('SECURE_AUTH_SALT', '@3;f2x/|i9b`Da3 4;Q>lgEX.g,6#Hhgjpk--InNYC9+|G=}D-uc`D=*|M~4O#^t');
define('LOGGED_IN_SALT',   '-?~Sh;TR06ysmi.8$Yw:m0jpmZZaQw%Y^TvB>-Q2bVX|X~}c#6h&Mz)6iL*/@ 38');
define('NONCE_SALT',       'a5RgIu3ESZhD8naArdg)j fq0*3f])Sy6Ifr{*ZL/3qId*I#uL?v,@%J~T;|.jPG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sh_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

define( 'FS_METHOD', 'direct' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
